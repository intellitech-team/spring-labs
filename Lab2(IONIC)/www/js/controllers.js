angular.module('starter.controllers', [])

.controller('LoginCtrl', function($scope,$state,$rootScope,$cookieStore,$http,
  $ionicLoading,LoginService) {
    $scope.credentials = {
        username: '', password: ''
    }
    var xAuthTokenHeaderName = 'x-auth-token';
    $scope.login = function() {
      $ionicLoading.show({
      template: 'Chargement...'
        });
      $scope.invalidCredentials=false;
        LoginService.authenticate({username: $scope.credentials.username, password: $scope.credentials.password})
         .$promise.then(function(user) {

          if(user.status=="OK"){
            $rootScope.user = user;
            $http.defaults.headers.common[ xAuthTokenHeaderName ] = user.token;
            $cookieStore.put('user', user);
            $state.go('tab.profile');
          }else{
            $scope.invalidCredentials=true;
          }
      
      $ionicLoading.hide();
      }, function(error) {
        $ionicLoading.hide();    
     });
    }
})

.controller('DeviceCtrl', function($scope) {

})

.controller('ProfileCtrl', function($scope) {
  
});

