angular.module('starter.services', [])

.service('LoginService', function($resource,SERVER_PATH) {
  return $resource('action:', {},
      {
        authenticate: {
          method: 'POST',
          url:SERVER_PATH.path+'/login/authenticate'
        }
      }
    );
})
;
