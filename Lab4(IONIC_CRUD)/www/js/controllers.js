angular.module('starter.controllers', [])

.controller('LoginCtrl', function($scope, $state, $rootScope, $cookieStore, $http,
   $ionicLoading, LoginService) {
    $scope.credentials = {
        username: '', password: ''
    }
    var xAuthTokenHeaderName = 'x-auth-token';
    $scope.login = function() {
      $ionicLoading.show({
      template: 'Chargement...'
        });
      $scope.invalidCredentials=false;
        LoginService.authenticate({username: $scope.credentials.username, password: $scope.credentials.password})
         .$promise.then(function(user) {

          if(user.status=="OK"){
            $rootScope.user = user;
            $http.defaults.headers.common[ xAuthTokenHeaderName ] = user.token;
            $cookieStore.put('user', user);
            $state.go('tab.profile');
          }else{
            $scope.invalidCredentials=true;
          }
      
      $ionicLoading.hide();
      }, function(error) {
        $ionicLoading.hide();    
     });
    }
})

.controller('DeviceCtrl', function($scope,$http, $ionicLoading, PopupService, DeviceService, SERVER_PATH) {

      $scope.devices=[];
      $scope.getAllDeviceOfUser=function(){
        $http.get(SERVER_PATH.path+"/devices/getByUserId")
      .success(function(data){
        
          $scope.devices=data;
          console.log($scope.devices)
      });
      }

      $scope.status=[
        {id:'1',_status:"ONLINE"},
        {id:'2',_status:"OFFLINE"}
      ];
      
      $scope.newDevice={};
      $scope.addNewDevice=function(){
         $ionicLoading.show({
         template: 'Chargement...'
         });
      
      DeviceService.addNewDevice({name:$scope.newDevice.name,uniqueId: $scope.newDevice.uniqueId,
        status:$scope.newDevice.status._status,phoneNumber:$scope.newDevice.phoneNumber})
        .$promise.then(function(response){
         
          PopupService.showPopup('Bien', 'Votre appareil a été ajouté','');
         
          $ionicLoading.hide(); 
         
       }, function(error) {
        console.log(error)
         $ionicLoading.hide();    
       });

      }


      $scope.deleteDevice=function(_deviceId){
        console.log(_deviceId)
        $ionicLoading.show({
         template: 'Chargement...'
         });

        DeviceService.deleteDevice({'deviceId':_deviceId})
        .$promise.then(function(user){
          $ionicLoading.hide(); 
          console.log(user);
         }, function(error) {
         $ionicLoading.hide();    
         });
      }
      
})

.controller('ProfileCtrl', function($scope, $ionicLoading, ProfileService,$http) {
    
    $scope.currentUser={};
    $scope.getCurrentUser=function(){
      // $ionicLoading.show({
      // template: 'Chargement...'
      //   });
      $http.get("http://localhost:8080/users/getCurrentUser")
      .success(function(data){
        $scope.currentUser=data;
          console.log(data);
      });
     //  ProfileService.getCurrentUser().$promise.then(function(user){
     //     $scope.currentUser=user;
     //     console.log(user);
     //  }, function(error) {
     //    $ionicLoading.hide();    
     // });
    }

    $scope.updateProfile=function(){
      
    }
});

